#!/usr/bin/env python3

fp = open("opcodes.txt", "r")

for line in fp:
  if line.startswith("#"): continue

  tokens = line.replace("(", "").replace(")", "").split()

  print("        case " + tokens[1] + ":")
  print("          name = \"" + tokens[2] + "\";")
  print("          System.out.println(\"Opcode (" + tokens[1] + ") " + tokens[2] + " unimplemented\");")
  print("          return;")
  print("          //break;")

fp.close()

