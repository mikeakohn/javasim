
Execute simple Java code step by step while showing the internals of the "JVM".

To test it with a simple class:

    java -cp JavaSim.jar:. JavaSim tests/Test.class

Source code is:

JavaSim.java: The main program.
JavaObject.java: Reads in a .class file and parses out methods and such.
Execute.java: Executes code from a method.

