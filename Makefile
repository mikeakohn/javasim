
default:
	@+make -C build
	@+make -C tests

clean:
	@rm -f build/*.class tests/*.class JavaSim.jar
	@echo "Clean!"

