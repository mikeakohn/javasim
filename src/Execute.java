
public class Execute
{
  static public void run(Method method)
  {
    Stack stack;
    byte[] code = method.getCode();
    int pc = 0;
    boolean is_wide = false;
    String name;

    method.dump();

    while (true)
    {
      int opcode = code[pc] & 0xff;

      name = "";

      // Opcode 0xc4 causes instructions to use mutiple bytes instead of a
      // a single byte for the opcode's data part. An example is
      // iload. Without wide it uses one byte, as wide it uses two.
      if (opcode == 0xc4)
      {
        is_wide = true;
        pc++;
        continue;
      }

      switch (opcode)
      {
        case 0x00:
          name = "nop";
          pc++;
          break;
        case 0x01:
          name = "aconst_null";
          System.out.println("Opcode (0x01) aconst_null unimplemented");
          return;
          //break;
        case 0x02:
          name = "iconst_m1";
          System.out.println("Opcode (0x02) iconst_m1 unimplemented");
          return;
          //break;
        case 0x03:
          name = "iconst_0";
          System.out.println("Opcode (0x03) iconst_0 unimplemented");
          return;
          //break;
        case 0x04:
          name = "iconst_1";
          System.out.println("Opcode (0x04) iconst_1 unimplemented");
          return;
          //break;
        case 0x05:
          name = "iconst_2";
          System.out.println("Opcode (0x05) iconst_2 unimplemented");
          return;
          //break;
        case 0x06:
          name = "iconst_3";
          System.out.println("Opcode (0x06) iconst_3 unimplemented");
          return;
          //break;
        case 0x07:
          name = "iconst_4";
          System.out.println("Opcode (0x07) iconst_4 unimplemented");
          return;
          //break;
        case 0x08:
          name = "iconst_5";
          System.out.println("Opcode (0x08) iconst_5 unimplemented");
          return;
          //break;
        case 0x09:
          name = "lconst_0";
          System.out.println("Opcode (0x09) lconst_0 unimplemented");
          return;
          //break;
        case 0x0a:
          name = "lconst_1";
          System.out.println("Opcode (0x0a) lconst_1 unimplemented");
          return;
          //break;
        case 0x0b:
          name = "fconst_0";
          System.out.println("Opcode (0x0b) fconst_0 unimplemented");
          return;
          //break;
        case 0x0c:
          name = "fconst_1";
          System.out.println("Opcode (0x0c) fconst_1 unimplemented");
          return;
          //break;
        case 0x0d:
          name = "fconst_2";
          System.out.println("Opcode (0x0d) fconst_2 unimplemented");
          return;
          //break;
        case 0x0e:
          name = "dconst_0";
          System.out.println("Opcode (0x0e) dconst_0 unimplemented");
          return;
          //break;
        case 0x0f:
          name = "dconst_1";
          System.out.println("Opcode (0x0f) dconst_1 unimplemented");
          return;
          //break;
        case 0x10:
          name = "bipush";
          System.out.println("Opcode (0x10) bipush unimplemented");
          return;
          //break;
        case 0x11:
          name = "sipush";
          System.out.println("Opcode (0x11) sipush unimplemented");
          return;
          //break;
        case 0x12:
          name = "ldc";
          System.out.println("Opcode (0x12) ldc unimplemented");
          return;
          //break;
        case 0x13:
          name = "ldc_w";
          System.out.println("Opcode (0x13) ldc_w unimplemented");
          return;
          //break;
        case 0x14:
          name = "ldc2_w";
          System.out.println("Opcode (0x14) ldc2_w unimplemented");
          return;
          //break;
        case 0x15:
          name = "iload";
          System.out.println("Opcode (0x15) iload unimplemented");
          return;
          //break;
        case 0x16:
          name = "lload";
          System.out.println("Opcode (0x16) lload unimplemented");
          return;
          //break;
        case 0x17:
          name = "fload";
          System.out.println("Opcode (0x17) fload unimplemented");
          return;
          //break;
        case 0x18:
          name = "dload";
          System.out.println("Opcode (0x18) dload unimplemented");
          return;
          //break;
        case 0x19:
          name = "aload";
          System.out.println("Opcode (0x19) aload unimplemented");
          return;
          //break;
        case 0x1a:
          name = "iload_0";
          System.out.println("Opcode (0x1a) iload_0 unimplemented");
          return;
          //break;
        case 0x1b:
          name = "iload_1";
          System.out.println("Opcode (0x1b) iload_1 unimplemented");
          return;
          //break;
        case 0x1c:
          name = "iload_2";
          System.out.println("Opcode (0x1c) iload_2 unimplemented");
          return;
          //break;
        case 0x1d:
          name = "iload_3";
          System.out.println("Opcode (0x1d) iload_3 unimplemented");
          return;
          //break;
        case 0x1e:
          name = "lload_0";
          System.out.println("Opcode (0x1e) lload_0 unimplemented");
          return;
          //break;
        case 0x1f:
          name = "lload_1";
          System.out.println("Opcode (0x1f) lload_1 unimplemented");
          return;
          //break;
        case 0x20:
          name = "lload_2";
          System.out.println("Opcode (0x20) lload_2 unimplemented");
          return;
          //break;
        case 0x21:
          name = "lload_3";
          System.out.println("Opcode (0x21) lload_3 unimplemented");
          return;
          //break;
        case 0x22:
          name = "fload_0";
          System.out.println("Opcode (0x22) fload_0 unimplemented");
          return;
          //break;
        case 0x23:
          name = "fload_1";
          System.out.println("Opcode (0x23) fload_1 unimplemented");
          return;
          //break;
        case 0x24:
          name = "fload_2";
          System.out.println("Opcode (0x24) fload_2 unimplemented");
          return;
          //break;
        case 0x25:
          name = "fload_3";
          System.out.println("Opcode (0x25) fload_3 unimplemented");
          return;
          //break;
        case 0x26:
          name = "dload_0";
          System.out.println("Opcode (0x26) dload_0 unimplemented");
          return;
          //break;
        case 0x27:
          name = "dload_1";
          System.out.println("Opcode (0x27) dload_1 unimplemented");
          return;
          //break;
        case 0x28:
          name = "dload_2";
          System.out.println("Opcode (0x28) dload_2 unimplemented");
          return;
          //break;
        case 0x29:
          name = "dload_3";
          System.out.println("Opcode (0x29) dload_3 unimplemented");
          return;
          //break;
        case 0x2a:
          name = "aload_0";
          System.out.println("Opcode (0x2a) aload_0 unimplemented");
          return;
          //break;
        case 0x2b:
          name = "aload_1";
          System.out.println("Opcode (0x2b) aload_1 unimplemented");
          return;
          //break;
        case 0x2c:
          name = "aload_2";
          System.out.println("Opcode (0x2c) aload_2 unimplemented");
          return;
          //break;
        case 0x2d:
          name = "aload_3";
          System.out.println("Opcode (0x2d) aload_3 unimplemented");
          return;
          //break;
        case 0x2e:
          name = "iaload";
          System.out.println("Opcode (0x2e) iaload unimplemented");
          return;
          //break;
        case 0x2f:
          name = "laload";
          System.out.println("Opcode (0x2f) laload unimplemented");
          return;
          //break;
        case 0x30:
          name = "faload";
          System.out.println("Opcode (0x30) faload unimplemented");
          return;
          //break;
        case 0x31:
          name = "daload";
          System.out.println("Opcode (0x31) daload unimplemented");
          return;
          //break;
        case 0x32:
          name = "aaload";
          System.out.println("Opcode (0x32) aaload unimplemented");
          return;
          //break;
        case 0x33:
          name = "baload";
          System.out.println("Opcode (0x33) baload unimplemented");
          return;
          //break;
        case 0x34:
          name = "caload";
          System.out.println("Opcode (0x34) caload unimplemented");
          return;
          //break;
        case 0x35:
          name = "saload";
          System.out.println("Opcode (0x35) saload unimplemented");
          return;
          //break;
        case 0x36:
          name = "istore";
          System.out.println("Opcode (0x36) istore unimplemented");
          return;
          //break;
        case 0x37:
          name = "lstore";
          System.out.println("Opcode (0x37) lstore unimplemented");
          return;
          //break;
        case 0x38:
          name = "fstore";
          System.out.println("Opcode (0x38) fstore unimplemented");
          return;
          //break;
        case 0x39:
          name = "dstore";
          System.out.println("Opcode (0x39) dstore unimplemented");
          return;
          //break;
        case 0x3a:
          name = "astore";
          System.out.println("Opcode (0x3a) astore unimplemented");
          return;
          //break;
        case 0x3b:
          name = "istore_0";
          System.out.println("Opcode (0x3b) istore_0 unimplemented");
          return;
          //break;
        case 0x3c:
          name = "istore_1";
          System.out.println("Opcode (0x3c) istore_1 unimplemented");
          return;
          //break;
        case 0x3d:
          name = "istore_2";
          System.out.println("Opcode (0x3d) istore_2 unimplemented");
          return;
          //break;
        case 0x3e:
          name = "istore_3";
          System.out.println("Opcode (0x3e) istore_3 unimplemented");
          return;
          //break;
        case 0x3f:
          name = "lstore_0";
          System.out.println("Opcode (0x3f) lstore_0 unimplemented");
          return;
          //break;
        case 0x40:
          name = "lstore_1";
          System.out.println("Opcode (0x40) lstore_1 unimplemented");
          return;
          //break;
        case 0x41:
          name = "lstore_2";
          System.out.println("Opcode (0x41) lstore_2 unimplemented");
          return;
          //break;
        case 0x42:
          name = "lstore_3";
          System.out.println("Opcode (0x42) lstore_3 unimplemented");
          return;
          //break;
        case 0x43:
          name = "fstore_0";
          System.out.println("Opcode (0x43) fstore_0 unimplemented");
          return;
          //break;
        case 0x44:
          name = "fstore_1";
          System.out.println("Opcode (0x44) fstore_1 unimplemented");
          return;
          //break;
        case 0x45:
          name = "fstore_2";
          System.out.println("Opcode (0x45) fstore_2 unimplemented");
          return;
          //break;
        case 0x46:
          name = "fstore_3";
          System.out.println("Opcode (0x46) fstore_3 unimplemented");
          return;
          //break;
        case 0x47:
          name = "dstore_0";
          System.out.println("Opcode (0x47) dstore_0 unimplemented");
          return;
          //break;
        case 0x48:
          name = "dstore_1";
          System.out.println("Opcode (0x48) dstore_1 unimplemented");
          return;
          //break;
        case 0x49:
          name = "dstore_2";
          System.out.println("Opcode (0x49) dstore_2 unimplemented");
          return;
          //break;
        case 0x4a:
          name = "dstore_3";
          System.out.println("Opcode (0x4a) dstore_3 unimplemented");
          return;
          //break;
        case 0x4b:
          name = "astore_0";
          System.out.println("Opcode (0x4b) astore_0 unimplemented");
          return;
          //break;
        case 0x4c:
          name = "astore_1";
          System.out.println("Opcode (0x4c) astore_1 unimplemented");
          return;
          //break;
        case 0x4d:
          name = "astore_2";
          System.out.println("Opcode (0x4d) astore_2 unimplemented");
          return;
          //break;
        case 0x4e:
          name = "astore_3";
          System.out.println("Opcode (0x4e) astore_3 unimplemented");
          return;
          //break;
        case 0x4f:
          name = "iastore";
          System.out.println("Opcode (0x4f) iastore unimplemented");
          return;
          //break;
        case 0x50:
          name = "lastore";
          System.out.println("Opcode (0x50) lastore unimplemented");
          return;
          //break;
        case 0x51:
          name = "fastore";
          System.out.println("Opcode (0x51) fastore unimplemented");
          return;
          //break;
        case 0x52:
          name = "dastore";
          System.out.println("Opcode (0x52) dastore unimplemented");
          return;
          //break;
        case 0x53:
          name = "aastore";
          System.out.println("Opcode (0x53) aastore unimplemented");
          return;
          //break;
        case 0x54:
          name = "bastore";
          System.out.println("Opcode (0x54) bastore unimplemented");
          return;
          //break;
        case 0x55:
          name = "castore";
          System.out.println("Opcode (0x55) castore unimplemented");
          return;
          //break;
        case 0x56:
          name = "sastore";
          System.out.println("Opcode (0x56) sastore unimplemented");
          return;
          //break;
        case 0x57:
          name = "pop";
          System.out.println("Opcode (0x57) pop unimplemented");
          return;
          //break;
        case 0x58:
          name = "pop2";
          System.out.println("Opcode (0x58) pop2 unimplemented");
          return;
          //break;
        case 0x59:
          name = "dup";
          System.out.println("Opcode (0x59) dup unimplemented");
          return;
          //break;
        case 0x5a:
          name = "dup_x1";
          System.out.println("Opcode (0x5a) dup_x1 unimplemented");
          return;
          //break;
        case 0x5b:
          name = "dup_x2";
          System.out.println("Opcode (0x5b) dup_x2 unimplemented");
          return;
          //break;
        case 0x5c:
          name = "dup2";
          System.out.println("Opcode (0x5c) dup2 unimplemented");
          return;
          //break;
        case 0x5d:
          name = "dup2_x1";
          System.out.println("Opcode (0x5d) dup2_x1 unimplemented");
          return;
          //break;
        case 0x5e:
          name = "dup2_x2";
          System.out.println("Opcode (0x5e) dup2_x2 unimplemented");
          return;
          //break;
        case 0x5f:
          name = "swap";
          System.out.println("Opcode (0x5f) swap unimplemented");
          return;
          //break;
        case 0x60:
          name = "iadd";
          System.out.println("Opcode (0x60) iadd unimplemented");
          return;
          //break;
        case 0x61:
          name = "ladd";
          System.out.println("Opcode (0x61) ladd unimplemented");
          return;
          //break;
        case 0x62:
          name = "fadd";
          System.out.println("Opcode (0x62) fadd unimplemented");
          return;
          //break;
        case 0x63:
          name = "dadd";
          System.out.println("Opcode (0x63) dadd unimplemented");
          return;
          //break;
        case 0x64:
          name = "isub";
          System.out.println("Opcode (0x64) isub unimplemented");
          return;
          //break;
        case 0x65:
          name = "lsub";
          System.out.println("Opcode (0x65) lsub unimplemented");
          return;
          //break;
        case 0x66:
          name = "fsub";
          System.out.println("Opcode (0x66) fsub unimplemented");
          return;
          //break;
        case 0x67:
          name = "dsub";
          System.out.println("Opcode (0x67) dsub unimplemented");
          return;
          //break;
        case 0x68:
          name = "imul";
          System.out.println("Opcode (0x68) imul unimplemented");
          return;
          //break;
        case 0x69:
          name = "lmul";
          System.out.println("Opcode (0x69) lmul unimplemented");
          return;
          //break;
        case 0x6a:
          name = "fmul";
          System.out.println("Opcode (0x6a) fmul unimplemented");
          return;
          //break;
        case 0x6b:
          name = "dmul";
          System.out.println("Opcode (0x6b) dmul unimplemented");
          return;
          //break;
        case 0x6c:
          name = "idiv";
          System.out.println("Opcode (0x6c) idiv unimplemented");
          return;
          //break;
        case 0x6d:
          name = "ldiv";
          System.out.println("Opcode (0x6d) ldiv unimplemented");
          return;
          //break;
        case 0x6e:
          name = "fdiv";
          System.out.println("Opcode (0x6e) fdiv unimplemented");
          return;
          //break;
        case 0x6f:
          name = "ddiv";
          System.out.println("Opcode (0x6f) ddiv unimplemented");
          return;
          //break;
        case 0x70:
          name = "irem";
          System.out.println("Opcode (0x70) irem unimplemented");
          return;
          //break;
        case 0x71:
          name = "lrem";
          System.out.println("Opcode (0x71) lrem unimplemented");
          return;
          //break;
        case 0x72:
          name = "frem";
          System.out.println("Opcode (0x72) frem unimplemented");
          return;
          //break;
        case 0x73:
          name = "drem";
          System.out.println("Opcode (0x73) drem unimplemented");
          return;
          //break;
        case 0x74:
          name = "ineg";
          System.out.println("Opcode (0x74) ineg unimplemented");
          return;
          //break;
        case 0x75:
          name = "lneg";
          System.out.println("Opcode (0x75) lneg unimplemented");
          return;
          //break;
        case 0x76:
          name = "fneg";
          System.out.println("Opcode (0x76) fneg unimplemented");
          return;
          //break;
        case 0x77:
          name = "dneg";
          System.out.println("Opcode (0x77) dneg unimplemented");
          return;
          //break;
        case 0x78:
          name = "ishl";
          System.out.println("Opcode (0x78) ishl unimplemented");
          return;
          //break;
        case 0x79:
          name = "lshl";
          System.out.println("Opcode (0x79) lshl unimplemented");
          return;
          //break;
        case 0x7a:
          name = "ishr";
          System.out.println("Opcode (0x7a) ishr unimplemented");
          return;
          //break;
        case 0x7b:
          name = "lshr";
          System.out.println("Opcode (0x7b) lshr unimplemented");
          return;
          //break;
        case 0x7c:
          name = "iushr";
          System.out.println("Opcode (0x7c) iushr unimplemented");
          return;
          //break;
        case 0x7d:
          name = "lushr";
          System.out.println("Opcode (0x7d) lushr unimplemented");
          return;
          //break;
        case 0x7e:
          name = "iand";
          System.out.println("Opcode (0x7e) iand unimplemented");
          return;
          //break;
        case 0x7f:
          name = "land";
          System.out.println("Opcode (0x7f) land unimplemented");
          return;
          //break;
        case 0x80:
          name = "ior";
          System.out.println("Opcode (0x80) ior unimplemented");
          return;
          //break;
        case 0x81:
          name = "lor";
          System.out.println("Opcode (0x81) lor unimplemented");
          return;
          //break;
        case 0x82:
          name = "ixor";
          System.out.println("Opcode (0x82) ixor unimplemented");
          return;
          //break;
        case 0x83:
          name = "lxor";
          System.out.println("Opcode (0x83) lxor unimplemented");
          return;
          //break;
        case 0x84:
          name = "iinc";
          System.out.println("Opcode (0x84) iinc unimplemented");
          return;
          //break;
        case 0x85:
          name = "i2l";
          System.out.println("Opcode (0x85) i2l unimplemented");
          return;
          //break;
        case 0x86:
          name = "i2f";
          System.out.println("Opcode (0x86) i2f unimplemented");
          return;
          //break;
        case 0x87:
          name = "i2d";
          System.out.println("Opcode (0x87) i2d unimplemented");
          return;
          //break;
        case 0x88:
          name = "l2i";
          System.out.println("Opcode (0x88) l2i unimplemented");
          return;
          //break;
        case 0x89:
          name = "l2f";
          System.out.println("Opcode (0x89) l2f unimplemented");
          return;
          //break;
        case 0x8a:
          name = "l2d";
          System.out.println("Opcode (0x8a) l2d unimplemented");
          return;
          //break;
        case 0x8b:
          name = "f2i";
          System.out.println("Opcode (0x8b) f2i unimplemented");
          return;
          //break;
        case 0x8c:
          name = "f2l";
          System.out.println("Opcode (0x8c) f2l unimplemented");
          return;
          //break;
        case 0x8d:
          name = "f2d";
          System.out.println("Opcode (0x8d) f2d unimplemented");
          return;
          //break;
        case 0x8e:
          name = "d2i";
          System.out.println("Opcode (0x8e) d2i unimplemented");
          return;
          //break;
        case 0x8f:
          name = "d2l";
          System.out.println("Opcode (0x8f) d2l unimplemented");
          return;
          //break;
        case 0x90:
          name = "d2f";
          System.out.println("Opcode (0x90) d2f unimplemented");
          return;
          //break;
        case 0x91:
          name = "i2b";
          System.out.println("Opcode (0x91) i2b unimplemented");
          return;
          //break;
        case 0x92:
          name = "i2c";
          System.out.println("Opcode (0x92) i2c unimplemented");
          return;
          //break;
        case 0x93:
          name = "i2s";
          System.out.println("Opcode (0x93) i2s unimplemented");
          return;
          //break;
        case 0x94:
          name = "lcmp";
          System.out.println("Opcode (0x94) lcmp unimplemented");
          return;
          //break;
        case 0x95:
          name = "fcmpl";
          System.out.println("Opcode (0x95) fcmpl unimplemented");
          return;
          //break;
        case 0x96:
          name = "fcmpg";
          System.out.println("Opcode (0x96) fcmpg unimplemented");
          return;
          //break;
        case 0x97:
          name = "dcmpl";
          System.out.println("Opcode (0x97) dcmpl unimplemented");
          return;
          //break;
        case 0x98:
          name = "dcmpg";
          System.out.println("Opcode (0x98) dcmpg unimplemented");
          return;
          //break;
        case 0x99:
          name = "ifeq";
          System.out.println("Opcode (0x99) ifeq unimplemented");
          return;
          //break;
        case 0x9a:
          name = "ifne";
          System.out.println("Opcode (0x9a) ifne unimplemented");
          return;
          //break;
        case 0x9b:
          name = "iflt";
          System.out.println("Opcode (0x9b) iflt unimplemented");
          return;
          //break;
        case 0x9c:
          name = "ifge";
          System.out.println("Opcode (0x9c) ifge unimplemented");
          return;
          //break;
        case 0x9d:
          name = "ifgt";
          System.out.println("Opcode (0x9d) ifgt unimplemented");
          return;
          //break;
        case 0x9e:
          name = "ifle";
          System.out.println("Opcode (0x9e) ifle unimplemented");
          return;
          //break;
        case 0x9f:
          name = "if_icmpeq";
          System.out.println("Opcode (0x9f) if_icmpeq unimplemented");
          return;
          //break;
        case 0xa0:
          name = "if_icmpne";
          System.out.println("Opcode (0xa0) if_icmpne unimplemented");
          return;
          //break;
        case 0xa1:
          name = "if_icmplt";
          System.out.println("Opcode (0xa1) if_icmplt unimplemented");
          return;
          //break;
        case 0xa2:
          name = "if_icmpge";
          System.out.println("Opcode (0xa2) if_icmpge unimplemented");
          return;
          //break;
        case 0xa3:
          name = "if_icmpgt";
          System.out.println("Opcode (0xa3) if_icmpgt unimplemented");
          return;
          //break;
        case 0xa4:
          name = "if_icmple";
          System.out.println("Opcode (0xa4) if_icmple unimplemented");
          return;
          //break;
        case 0xa5:
          name = "if_acmpeq";
          System.out.println("Opcode (0xa5) if_acmpeq unimplemented");
          return;
          //break;
        case 0xa6:
          name = "if_acmpne";
          System.out.println("Opcode (0xa6) if_acmpne unimplemented");
          return;
          //break;
        case 0xa7:
          name = "goto";
          System.out.println("Opcode (0xa7) goto unimplemented");
          return;
          //break;
        case 0xa8:
          name = "jsr";
          System.out.println("Opcode (0xa8) jsr unimplemented");
          return;
          //break;
        case 0xa9:
          name = "ret";
          System.out.println("Opcode (0xa9) ret unimplemented");
          return;
          //break;
        case 0xaa:
          name = "tableswitch";
          System.out.println("Opcode (0xaa) tableswitch unimplemented");
          return;
          //break;
        case 0xab:
          name = "lookupswitch";
          System.out.println("Opcode (0xab) lookupswitch unimplemented");
          return;
          //break;
        case 0xac:
          name = "ireturn";
          System.out.println("Opcode (0xac) ireturn unimplemented");
          return;
          //break;
        case 0xad:
          name = "lreturn";
          System.out.println("Opcode (0xad) lreturn unimplemented");
          return;
          //break;
        case 0xae:
          name = "freturn";
          System.out.println("Opcode (0xae) freturn unimplemented");
          return;
          //break;
        case 0xaf:
          name = "dreturn";
          System.out.println("Opcode (0xaf) dreturn unimplemented");
          return;
          //break;
        case 0xb0:
          name = "areturn";
          System.out.println("Opcode (0xb0) areturn unimplemented");
          return;
          //break;
        case 0xb1:
          name = "return";
          System.out.println("Opcode (0xb1) return unimplemented");
          return;
          //break;
        case 0xb2:
          name = "getstatic";
          System.out.println("Opcode (0xb2) getstatic unimplemented");
          return;
          //break;
        case 0xb3:
          name = "putstatic";
          System.out.println("Opcode (0xb3) putstatic unimplemented");
          return;
          //break;
        case 0xb4:
          name = "getfield";
          System.out.println("Opcode (0xb4) getfield unimplemented");
          return;
          //break;
        case 0xb5:
          name = "putfield";
          System.out.println("Opcode (0xb5) putfield unimplemented");
          return;
          //break;
        case 0xb6:
          name = "invokevirtual";
          System.out.println("Opcode (0xb6) invokevirtual unimplemented");
          return;
          //break;
        case 0xb7:
          name = "invokespecial";
          System.out.println("Opcode (0xb7) invokespecial unimplemented");
          return;
          //break;
        case 0xb8:
          name = "invokestatic";
          System.out.println("Opcode (0xb8) invokestatic unimplemented");
          return;
          //break;
        case 0xb9:
          name = "invokeinterface";
          System.out.println("Opcode (0xb9) invokeinterface unimplemented");
          return;
          //break;
        case 0xba:
          name = "xxxunusedxxx1";
          System.out.println("Opcode (0xba) xxxunusedxxx1 unimplemented");
          return;
          //break;
        case 0xbb:
          name = "new";
          System.out.println("Opcode (0xbb) new unimplemented");
          return;
          //break;
        case 0xbc:
          name = "newarray";
          System.out.println("Opcode (0xbc) newarray unimplemented");
          return;
          //break;
        case 0xbd:
          name = "anewarray";
          System.out.println("Opcode (0xbd) anewarray unimplemented");
          return;
          //break;
        case 0xbe:
          name = "arraylength";
          System.out.println("Opcode (0xbe) arraylength unimplemented");
          return;
          //break;
        case 0xbf:
          name = "athrow";
          System.out.println("Opcode (0xbf) athrow unimplemented");
          return;
          //break;
        case 0xc0:
          name = "checkcast";
          System.out.println("Opcode (0xc0) checkcast unimplemented");
          return;
          //break;
        case 0xc1:
          name = "instanceof";
          System.out.println("Opcode (0xc1) instanceof unimplemented");
          return;
          //break;
        case 0xc2:
          name = "monitorenter";
          System.out.println("Opcode (0xc2) monitorenter unimplemented");
          return;
          //break;
        case 0xc3:
          name = "monitorexit";
          System.out.println("Opcode (0xc3) monitorexit unimplemented");
          return;
          //break;
        case 0xc5:
          name = "multianewarray";
          System.out.println("Opcode (0xc5) multianewarray unimplemented");
          return;
          //break;
        case 0xc6:
          name = "ifnull";
          System.out.println("Opcode (0xc6) ifnull unimplemented");
          return;
          //break;
        case 0xc7:
          name = "ifnonnull";
          System.out.println("Opcode (0xc7) ifnonnull unimplemented");
          return;
          //break;
        case 0xc8:
          name = "goto_w";
          System.out.println("Opcode (0xc8) goto_w unimplemented");
          return;
          //break;
        case 0xc9:
          name = "jsr_w";
          System.out.println("Opcode (0xc9) jsr_w unimplemented");
          return;
          //break;
        case 0xca:
          name = "breakpoint";
          System.out.println("Opcode (0xca) breakpoint unimplemented");
          return;
          //break;
        case 0xfe:
          name = "impdep1";
          System.out.println("Opcode (0xfe) impdep1 unimplemented");
          return;
          //break;
        case 0xff:
          name = "impdep2";
          System.out.println("Opcode (0xff) impdep2 unimplemented");
          return;
          //break;
        default:
          System.out.println("Opcode " + opcode + " ??? unimplemented");
          return;
      }

      is_wide = false;

      System.out.println("Executed " + name + " pc=" + pc);
    }
  }
}

