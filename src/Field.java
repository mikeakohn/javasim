
public class Field
{
  public String getAccessFlags()
  {
    String value = "";

    if ((access_flags & PUBLIC) != 0) { value += "public "; }
    if ((access_flags & PRIVATE) != 0) { value += "private "; }
    if ((access_flags & PROTECTED) != 0) { value += "protected "; }
    if ((access_flags & STATIC ) != 0) { value += "static "; }
    if ((access_flags & FINAL) != 0) { value += "final "; }
    if ((access_flags & VOLATILE) != 0) { value += "volatile "; }
    if ((access_flags & TRANSIENT) != 0) { value += "transient "; }
    if ((access_flags & SYNTHETIC) != 0) { value += "synthetic "; }
    if ((access_flags & ENUM) != 0) { value += "enum "; }

    return value;
  }

  public int access_flags;
  public String name;
  public String type;
  public int attributes_count;

  public static final int PUBLIC = 0x0001;
  public static final int PRIVATE = 0x0002;
  public static final int PROTECTED = 0x0004;
  public static final int STATIC = 0x0008;
  public static final int FINAL = 0x0010;
  public static final int VOLATILE = 0x0040;
  public static final int TRANSIENT = 0x0080;
  public static final int SYNTHETIC = 0x1000;
  public static final int ENUM = 0x4000;
}

