
public class Method
{
  public Method() { }

  public String getName() { return name; }
  public int getStackSize() { return max_stack; }
  public int getLocalVariableCount() { return max_locals; }
  public byte[] getCode() { return code; }

  public String getAccessFlags()
  {
    String value = "";

    if ((access_flags & PUBLIC) != 0) { value += "public "; }
    if ((access_flags & PRIVATE) != 0) { value += "private "; }
    if ((access_flags & PROTECTED) != 0) { value += "protected "; }
    if ((access_flags & STATIC) != 0) { value += "static "; }
    if ((access_flags & FINAL) != 0) { value += "final "; }
    if ((access_flags & SYNCHRONIZED) != 0) { value += "synchronized "; }
    if ((access_flags & BRIDGE) != 0) { value += "bridge "; }
    if ((access_flags & VARARGS) != 0) { value += "varargs "; }
    if ((access_flags & NATIVE) != 0) { value += "native "; }
    if ((access_flags & ABSTRACT) != 0) { value += "abstract "; }
    if ((access_flags & STRICT) != 0) { value += "strict "; }
    if ((access_flags & SYNTHETIC) != 0) { value += "synthetic "; }

    return value;
  }

  public void dump()
  {
    System.out.println("-- Method " + name + " " + type + " --");
    System.out.println(" max_stack: " + max_stack);
    System.out.println("max_locals: " + max_locals);

    for (int i = 0; i < code_length; i++)
    {
      System.out.printf(" %02x", code[i]);
      if (((i + 1) % 8) == 0) { System.out.println(); }
    }

    System.out.println();
  }

  public int access_flags;
  public String name;
  public String type;
  public String attributes_count;

  // This will be maximum stack length.
  int max_stack;

  // The number of local variables in this class. Method parameters
  // are counted as local variables.
  int max_locals;

  // Should be equal to length of code array.
  int code_length;

  public byte[] code;

  public static final int PUBLIC = 0x0001;
  public static final int PRIVATE = 0x0002;
  public static final int PROTECTED = 0x0004;
  public static final int STATIC = 0x0008;
  public static final int FINAL = 0x0010;
  public static final int SYNCHRONIZED = 0x0020;
  public static final int BRIDGE = 0x0040;
  public static final int VARARGS = 0x0080;
  public static final int NATIVE = 0x0100;
  public static final int ABSTRACT = 0x0400;
  public static final int STRICT = 0x0800;
  public static final int SYNTHETIC = 0x1000;
}

