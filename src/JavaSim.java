
public class JavaSim
{
  static public void main(String args[])
  {
    if (args.length != 1)
    {
      System.out.println("Usage: java JavaSim <classfile.class>");
      System.exit(-1);
    }

    JavaObject java_object = new JavaObject();
    java_object.read(args[0]);
    java_object.dump();

    Method method = java_object.getMethod("main");

    Execute.run(method);
  }
}

