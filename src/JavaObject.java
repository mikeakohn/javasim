import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Vector;

public class JavaObject
{
  public JavaObject()
  {
  }

  public Method getMethod(String name)
  {
    for (int i = 0; i < method_count; i++)
    {
      Method method = methods.get(i);

      if (name.equals(method.getName())) { return method; }
    }

    return null;
  }

  public void read(String filename)
  {
    int i, offset;
    int tag, class_index, name_and_type;
    int name_index, descriptor_index, attr_count;

    this.filename = filename;

    try
    {
      data = Files.readAllBytes(Paths.get(filename));
    }
    catch (IOException e)
    {
      System.out.println(e.toString());
      return;
    }

    magic_number = getInt(0);
    minor_version = getShort(4);
    major_version = getShort(6);
    constant_count = getShort(8);

    offset = readConstants(10);

    int access_flags = getShort(offset);
    int this_class = getShort(offset + 2);
    int super_class = getShort(offset + 4);
    offset += 6;

    // this_class will be the index of a "class" constant which will
    // have the index to the class name.
    this_class = constants.get(this_class).value_1;
    this.this_class = constants.get(this_class).text;

    // super_class will be the index of a "class" constant which will
    // have the index to the class name.
    super_class = constants.get(super_class).value_1;
    this.super_class = constants.get(super_class).text;

    interface_count = getShort(offset);
    offset += 2;

    for (i = 0; i < interface_count; i++)
    {
      // FIXME: Need to implement interfaces.
/*
      Method method = new Method();

      tag = data[offset];
      class_index = getShort(offset + 1);
      name_and_type = getShort(offset + 3);

      offset += 5;

      interfaces.add(method);
*/
    }

    field_count = getShort(offset);
    offset += 2;

    for (i = 0; i < field_count; i++)
    {
      Field field = new Field();

      field.access_flags = getShort(offset);
      name_index = getShort(offset + 2);
      descriptor_index = getShort(offset + 4);
      // FIXME: Attribute count is ignored now. Could cause problems.
      attr_count = getShort(offset + 6);

      field.name = constants.get(name_index).text;
      field.type = constants.get(descriptor_index).text;

      offset += 8;

      fields.add(field);
    }

    method_count = getShort(offset);
    offset += 2;

    for (i = 0; i < method_count; i++)
    {
      Method method = new Method();

      method.access_flags = getShort(offset);
      name_index = getShort(offset + 2);
      descriptor_index = getShort(offset + 4);
      attr_count = getShort(offset + 6);

      method.name = constants.get(name_index).text;
      method.type = constants.get(descriptor_index).text;

      offset += 8;

      for (int n = 0; n < attr_count; n++)
      {
        String name = constants.get(getShort(offset)).text;
        int length = getInt(offset + 2);

        offset += 6;

        if (name.equals("Code"))
        {
          method.max_stack = getShort(offset);
          method.max_locals = getShort(offset + 2);
          method.code_length = getInt(offset + 4);
          method.code = Arrays.copyOfRange(data, offset + 8, offset + 8 + method.code_length);

System.out.println(method.max_stack + " " + method.max_locals + " " + method.code_length);

        }

        offset += length;
      }

      methods.add(method);
    }

    // TODO: Ignoring attributes for now.
    attribute_count = getShort(offset);
    offset += 2;
  }

  public int readConstants(int offset)
  {
    int count;

    // Odd bug in the Java file format.
    constants.add(new Constant());

    for (int i = 1; i < constant_count; i++)
    {
      int tag = data[offset];

      Constant constant = new Constant();
      constant.tag = tag;

      offset++;

      switch (tag)
      {
        case 1:
          // UTF-8
          count = getShort(offset);
          offset += 2;
          constant.text =
            new String(Arrays.copyOfRange(data, offset, offset + count));
          offset += count;
          break;
        case 3:
          // Integer
          constant.integer = getInt(offset);
          offset += 4;
          break;
        case 4:
          // Float
          constant.real = getFloat(offset);
          offset += 4;
          break;
        case 5:
          // Long
          constant.integer64 = getLong(offset);
          offset += 8;
          break;
        case 6:
          // Double
          constant.real = getDouble(offset);
          offset += 8;
          break;
        case 7:
          // Class
          constant.value_1 = getShort(offset);
          offset += 2;
          break;
        case 8:
          // String
          constant.value_1 = getShort(offset);
          offset += 2;
          break;
        case 9:
          // FieldRef
          constant.value_1 = getShort(offset + 0);
          constant.value_2 = getShort(offset + 2);
          offset += 4;
          break;
        case 10:
          // Methodref
          constant.value_1 = getShort(offset + 0);
          constant.value_2 = getShort(offset + 2);
          offset += 4;
          break;
        case 11:
          // InterfaceMethodref
          constant.value_1 = getShort(offset + 0);
          constant.value_2 = getShort(offset + 2);
          offset += 4;
          break;
        case 12:
          // NameAndRef
          constant.value_1 = getShort(offset + 0);
          constant.value_2 = getShort(offset + 2);
          offset += 4;
          break;
        case 15:
          constant.value_1 = data[offset];
          constant.value_2 = getShort(offset + 1);
          offset += 3;
          break;
        case 16:
          // MethodType
          constant.value_1 = getShort(offset);
          offset += 2;
          break;
        case 18:
          // InvokeDynamic
          constant.value_1 = getInt(offset);
          offset += 4;
          break;
        default:
          System.out.println("Unknown constant tag " + tag + " at " + offset);
          break;
      }

      constants.add(constant);
    }

    return offset;
  }

  public void dump()
  {
    int i;

    System.out.println("         Name: " + filename);
    System.out.println("       Length: " + data.length);
    System.out.printf(" Magic Number: %08x\n", magic_number);
    System.out.println("Minor Version: " + minor_version);
    System.out.println("Major Version: " + major_version);
    System.out.println("    Constants: " + constant_count);
    dumpConstants();
    System.out.println(" Access Flags: " + access_flags);
    System.out.println("   This Class: " + this_class);
    System.out.println("  Super Class: " + super_class);

    System.out.println("   Interfaces: " + interface_count);

    for (i = 0; i < interface_count; i++)
    {
      Method method = interfaces.get(i);

      System.out.println(i + ") " +
        method.getAccessFlags() + " " +
        method.name);
    }

    System.out.println("       Fields: " + field_count);

    for (i = 0; i < field_count; i++)
    {
      Field field = fields.get(i);

      System.out.println(i + ") " +
        field.getAccessFlags() + " " +
        "name=" + field.name + " " +
        "type=" + field.type);
    }

    System.out.println("      Methods: " + method_count);

    for (i = 0; i < method_count; i++)
    {
      Method method = methods.get(i);

      System.out.println(i + ") " +
        method.getAccessFlags() + " " +
        "name=" + method.name + " " +
        "type=" + method.type);
    }

    System.out.println("   Attributes: " + attribute_count);
  }

  public void dumpConstants()
  {
    for (int i = 1; i < constant_count; i++)
    {
      Constant constant = constants.get(i); 

      switch (constant.tag)
      {
        case 1:
          System.out.println(i + ") UTF-8: " + constant.text);
          break;
        case 3:
          System.out.println(i + ") Integer: " + constant.integer);
          break;
        case 4:
          System.out.println(i + ") Float: " + constant.real);
          break;
        case 5:
          System.out.println(i + ") Long: " + constant.integer64);
          break;
        case 6:
          System.out.println(i + ") Double: " + constant.real);
          break;
        case 7:
          System.out.println(i + ") Class: " + constant.value_1);
          break;
        case 8:
          System.out.println(i + ") String: " + constant.value_1);
          break;
        case 9:
          System.out.println(i + ") Fieldref: " +
            "class=" + constant.value_1 + " " +
            "name_type=" + constant.value_2);
          break;
        case 10:
          System.out.println(i + ") Methodref: " +
            "class=" + constant.value_1 + " " +
            "name_type=" + constant.value_2);
          break;
        case 11:
          System.out.println(i + ") InterfaceMethodref: " +
            "class=" + constant.value_1 + " " +
            "name_type=" + constant.value_2);
          break;
        case 12:
          System.out.println(i + ") NameAndType: " +
            "name=" + constant.value_1 + " " +
            "type=" + constant.value_2);
          break;
        case 15:
          System.out.println(i + ") MethodHandle: " +
            "type=" + constant.value_1 + " " +
            "constant=" + constant.value_2);
          break;
        case 16:
          System.out.println(i + ") MethodType: " + "type=" + constant.value_1);
          break;
        case 18:
          System.out.println(i + ") InvokeDynamic: " + "data=" +
            constant.value_1);
          break;
        default:
          System.out.println(i + ") ** Unknown constant tag " + constant.tag);
          break;
      }
    }
  }

  private int getInt(int offset)
  {
    return (((int)data[offset + 0] & 0xff) << 24) |
           (((int)data[offset + 1] & 0xff) << 16) |
           (((int)data[offset + 2] & 0xff) << 8) |
            ((int)data[offset + 3] & 0xff);
  }

  private int getShort(int offset)
  {
    return (((int)data[offset + 0] & 0xff) << 8) |
            ((int)data[offset + 1] & 0xff);
  }

  private float getFloat(int offset)
  {
    byte[] bytes = Arrays.copyOfRange(data, offset, offset + 4);
    return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getFloat();
  }

  private double getDouble(int offset)
  {
    byte[] bytes = Arrays.copyOfRange(data, offset, offset + 8);
    return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getDouble();
  }

  private long getLong(int offset)
  {
    byte[] bytes = Arrays.copyOfRange(data, offset, offset + 8);
    return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getLong();
  }

  private byte[] data;
  private String filename;
  private int magic_number;
  private int minor_version;
  private int major_version;
  private int constant_count;
  private Vector<Constant> constants = new Vector<Constant>();
  private Vector<Method> interfaces = new Vector<Method>();
  private Vector<Field> fields = new Vector<Field>();
  private Vector<Method> methods = new Vector<Method>();
  private int access_flags;
  private String this_class;
  private String super_class;
  private int interface_count;
  private int field_count;
  private int method_count;
  private int attribute_count;
}

